const PUBLIC_FEED = 'https://www.flickr.com/services/feeds/photos_public.gne';
const FRIEND_FEED = 'https://www.flickr.com/services/feeds/photos_friends.gne';
const FAVES_FEED = 'https://www.flickr.com/services/feeds/photos_faves.gne';
const DISCUSSIONS_FEED = 'https://www.flickr.com/services/feeds/groups_discuss.gne';
const POOLS_FEED = 'https://www.flickr.com/services/feeds/groups_pool.gne';
const FORUM_POSTS = 'https://www.flickr.com/services/feeds/forums.gne';
const PHOTO_COMMENTS = 'https://www.flickr.com/services/feeds/photos_comments.gne';

module.exports = {
  PUBLIC_FEED,
  FRIEND_FEED,
  FAVES_FEED,
  DISCUSSIONS_FEED,
  POOLS_FEED,
  FORUM_POSTS,
  PHOTO_COMMENTS,
};
