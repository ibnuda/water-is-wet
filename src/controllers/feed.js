const {publicFeed} = require('../services/flickr');
const {feedTransformer} = require('../utils/transformer');
const {paramFilters} = require('../utils/misc');

const getFeed = async (req, res) => {
  try {
    const queryParams = req.query;
    const params = paramFilters(queryParams);
    const response = await publicFeed(params);
    const jsonFlickrFeed = feedTransformer(response.data);
    res.json(jsonFlickrFeed);
  } catch (error) {
    res.status(500).send({error, message: 'cannot fetch feed.'});
  }
};

module.exports = {
  getFeed,
};
