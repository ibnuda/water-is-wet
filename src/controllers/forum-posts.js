const {forumPosts} = require('../services/flickr');
const {feedTransformer} = require('../utils/transformer');
const {paramFilters} = require('../utils/misc');

const getForumPosts = async (req, res) => {
  try {
    const id = req.params.id;
    const queryParams = req.query;
    const params = paramFilters(queryParams);
    const response = await forumPosts(id, params);
    const jsonFlickrFeed = feedTransformer(response.data);
    res.json(jsonFlickrFeed);
  } catch (error) {
    res.status(500).send({error, message: 'cannot fetch feed.'});
  }
};

module.exports = {
  getForumPosts,
};
