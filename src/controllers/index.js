const {getFeed} = require('./feed');
const {getFriendsFeed} = require('./friends-feed');
const {getFavesFeed} = require('./faves-feed');
const {getDiscussions} = require('./discussions');
const {getPools} = require('./pools');
const {getForumPosts} = require('./forum-posts');
const {getComments} = require('./comments');

module.exports = {
  getFeed,
  getFriendsFeed,
  getFavesFeed,
  getDiscussions,
  getPools,
  getForumPosts,
  getComments,
};
