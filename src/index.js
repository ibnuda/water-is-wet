const express = require('express');
const cors = require('cors');
const app = express();

const {
  getFeed,
  getFriendsFeed,
  getFavesFeed,
  getDiscussions,
  getPools,
  getForumPosts,
  getComments,
} = require('./controllers');

app.use(cors({origin: 'http://localhost:8000'}));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/', getFeed);
app.get('/friends/:id', getFriendsFeed);
app.get('/faves/:id', getFavesFeed);
app.get('/discussions/:id', getDiscussions);
app.get('/pools/:id', getPools);
app.get('/forum', getForumPosts);
app.get('/comments/:id', getComments);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log('server is running');
});
