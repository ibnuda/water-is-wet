const axios = require('axios');
const {
  PUBLIC_FEED,
  FRIEND_FEED,
  FAVES_FEED,
  DISCUSSIONS_FEED,
  POOLS_FEED,
  FORUM_POSTS,
  PHOTO_COMMENTS,
} = require('../constants');

const getFeed = async (url, params) => {
  const uriparam = new URLSearchParams(params).toString();
  return await axios.get(`${url}?${uriparam}`);
};

const publicFeed = async (params) => getFeed(PUBLIC_FEED, params);
const friendsFeed = async (id, params) =>
  getFeed(FRIEND_FEED, {user_id: id, ...params});
const favesFeed = async (id, params) => getFeed(FAVES_FEED, {id, ...params});
const discussions = async (id, params) =>
  getFeed(DISCUSSIONS_FEED, {id, ...params});
const pools = async (id, params) => getFeed(POOLS_FEED, {id, ...params});
const forumPosts = async (id, params) => getFeed(FORUM_POSTS, {id, ...params});
const comments = async (id, params) =>
  getFeed(PHOTO_COMMENTS, {user_id: id, ...params});

module.exports = {
  publicFeed,
  friendsFeed,
  favesFeed,
  discussions,
  pools,
  forumPosts,
  comments,
};
