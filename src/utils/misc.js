const toArray = (thing) => {
  if (Array.isArray(thing)) {
    return thing;
  }
  return [thing];
};

const paramFilters = (params) => {
  // we are putting almost all params here because flickr will just ignore
  // unsupported params.
  return {
    ...(params.id && {id: params.id}),
    ...(params.ids && {ids: params.ids}),
    ...(params.tags && {tag: params.tags}),
    ...(params.tagmode && {tagmode: params.tagmode}),
    ...(params.user_id && {user_id: params.user_id}),
    ...(params.display_all && {display_all: params.display_all}),
    ...(params.friends && {friends: params.friends}),
  };
};

module.exports = {
  toArray,
  paramFilters,
};
