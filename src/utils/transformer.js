const {XMLParser} = require('fast-xml-parser');

const {toArray} = require('./misc');

const options = {
  ignoreAttributes: false,
};

const parser = new XMLParser(options);

const linkMapper = (link) => {
  return {
    rel: link['@_rel'],
    type: link['@_type'],
    href: link['@_href'],
  };
};

const contentMapper = (content) => {
  return {
    type: content['@_type'],
    text: content['#text'],
  };
};

const authorMapper = (author) => {
  return {
    name: author.name,
    uri: author.uri,
    nsid: author['flickr:nsid'],
    buddyicon: author['flickr:buddyicon'],
  };
};

const categoryMapper = (category) => {
  return category['@_term'] ?
  category['@_term'] :
  null;
};

const entryMapper = (entry) => {
  const title = entry.title;
  const id = entry.id;
  const published = entry.published;
  const updated = entry.updated;
  const link = entry.link ? toArray(entry.link).map(linkMapper) : null;
  const content = contentMapper(entry.content);
  const author = authorMapper(entry.author);
  const category = entry.category ?
    toArray(entry.category).map(categoryMapper).filter((x) => x) :
    null;
  return {
    title, id, published, updated, link, content, author, category,
  };
};

const feedTransformer = (xml) => {
  console.log(xml);
  try {
    const parsedXMl = parser.parse(xml);
    const feed = parsedXMl.feed;
    const title = feed.title;
    const subtitle = feed.subtitle;
    const updated = feed.updated;
    const entry = feed.entry.map(entryMapper);
    return {
      title,
      subtitle,
      updated,
      entry,
    };
  } catch (error) {
    throw error;
  }
};

module.exports = {
  feedTransformer,
};
